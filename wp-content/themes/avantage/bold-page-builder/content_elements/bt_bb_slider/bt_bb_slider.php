<?php

class bt_bb_slider extends BT_BB_Element {
	
	public $auto_play = '';

	function handle_shortcode( $atts, $content ) {
		extract( shortcode_atts( apply_filters( 'bt_bb_extract_atts_' . $this->shortcode, array(
			'images'    			=> '',
			'height'    			=> '',
			'show_arrows'     		=> '',
			'show_dots'     		=> '',
			'dots_style' 			=> '',
			'animation' 			=> '',
			'arrows_size' 			=> '',
			'arrows_style' 			=> '',
			'arrows_position'		=> '',
			'slides_to_show' 		=> '',
			'auto_play' 			=> ''
		) ), $atts, $this->shortcode ) );
		
		$slider_class = array( 'slick-slider' );
		$class = array( $this->shortcode );
		
		if ( $el_class != '' ) {
			$class[] = $el_class;
		}
		
		$id_attr = '';
		if ( $el_id != '' ) {
			$id_attr = ' ' . 'id="' . esc_attr( $el_id ) . '"';
		}

		$style_attr = '';
		if ( $el_style != '' ) {
			$style_attr = ' ' . 'style="' . esc_attr( $el_style ) . '"';
		}

		if ( $height != '' ) {
			$class[] = $this->prefix . 'height' . '_' . $height;
		}	
		
		$data_slick = ' ' . 'data-slick=\'{ "lazyLoad": "progressive", "cssEase": "ease-out", "speed": "300"';
		
		if ( $animation == 'fade' ) {
			$data_slick .= ', "fade": true';
			$slider_class[] = 'fade';
			$slides_to_show = 1;
		}
		
		if ( $height != 'keep-height' ) {
			$data_slick .= ', "adaptiveHeight": true';
		}
		
		if ( $show_dots != 'hide' ) {
			$data_slick .= ', "dots": true' ;
			$class[] = $this->prefix . 'show_dots' . '_' . $show_dots;
		}

		if ( $dots_style != '' ) {
			$class[] = $this->prefix . 'dots_style_' . $dots_style;
		}

		if ( $show_arrows != 'hide' ) {
			$data_slick  .= ', "prevArrow": "&lt;button type=\"button\" class=\"slick-prev\"&gt;", "nextArrow": "&lt;button type=\"button\" class=\"slick-next\"&gt;"';
		} else {
			$data_slick  .= ', "prevArrow": "", "nextArrow": ""';
		}

		if ( $arrows_size != '' ) {
			$class[] = $this->prefix . 'arrows_size' . '_' . $arrows_size;
		}
		
		if ( $arrows_style != '' ) {
			$class[] = $this->prefix . 'arrows_style' . '_' . $arrows_style;
		}
		
		if ( $arrows_position != '' ) {
			$class[] = $this->prefix . 'arrows_position' . '_' . $arrows_position;
		}
		
		if ( $slides_to_show > 1 ) {
			$data_slick .= ',"slidesToShow": ' . intval( $slides_to_show );
			$class[] = $this->prefix . 'multiple_slides';
		}
		
		if ( $auto_play != '' ) {
			$data_slick .= ',"autoplay": true, "autoplaySpeed": ' . intval( $auto_play );
		}
		
		if ( is_rtl() ) {
			$data_slick .= ', "rtl": true' ;
		}
		
		if ( $slides_to_show > 1 ) {
			$data_slick .= ', "responsive": [';
			if ( $slides_to_show > 1 ) {
				$data_slick .= '{ "breakpoint": 480, "settings": { "slidesToShow": 1, "slidesToScroll": 1 } }';	
			}
			if ( $slides_to_show > 2 ) {
				$data_slick .= ',{ "breakpoint": 768, "settings": { "slidesToShow": 2, "slidesToScroll": 2 } }';	
			}
			if ( $slides_to_show > 3 ) {
				$data_slick .= ',{ "breakpoint": 920, "settings": { "slidesToShow": 3, "slidesToScroll": 3 } }';	
			}
			if ( $slides_to_show > 4 ) {
				$data_slick .= ',{ "breakpoint": 1024, "settings": { "slidesToShow": 3, "slidesToScroll": 3 } }';	
			}				
			$data_slick .= ']';
		}
		
		$data_slick = $data_slick . '}\' ';
		
		$class = apply_filters( $this->shortcode . '_class', $class, $atts );
		
		$output = '';

		if ( $images != '' ) {
			$image_array = explode( ',', $images );
			foreach( $image_array as $image ) {
				$caption = "";
				if ( is_numeric($image) ) {
					$post_image = get_post( $image );
					if ( is_object( $post_image ) ) {
						$caption = $post_image->post_excerpt;
						$image = wp_get_attachment_image_src( $image, 'full' );
						$image = $image[0];
					} else {
						$image = plugins_url( 'placeholder.png', __FILE__ );
					}
				}
				if ( $height == 'auto' || $height == 'keep-height' ) {
					$output .= '<div class="bt_bb_slider_item"><img src="' . esc_url_raw( $image ) . '" alt="' . esc_attr( $caption ) . '"></div>';
				} else {
					$output .= '<div class="bt_bb_slider_item" style="background-image:url(\'' . esc_url_raw( $image ) . '\')"></div>';
				}
				
			}
		}

		$output = '<div' . $id_attr . ' class="' . implode( ' ', $class ) . '"' . $style_attr . '><div class="' . esc_attr( implode( ' ', $slider_class ) ) . '" ' . $data_slick . '>' . $output . '</div></div>';
		
		$output = apply_filters( 'bt_bb_general_output', $output, $atts );
		$output = apply_filters( $this->shortcode . '_output', $output, $atts );
		
		return $output;

	}

	function map_shortcode() {
		bt_bb_map( $this->shortcode, array( 'name' => esc_html__( 'Image Slider', 'avantage' ), 'description' => esc_html__( 'Slider with images', 'avantage' ), 'icon' => $this->prefix_backend . 'icon' . '_' . $this->shortcode,
			'params' => array(
				array( 'param_name' => 'images', 'type' => 'attach_images', 'heading' => esc_html__( 'Images', 'avantage' ) ),
				array( 'param_name' => 'height', 'type' => 'dropdown', 'heading' => esc_html__( 'Height', 'avantage' ),
					'value' => array(
						esc_html__('Auto', 'avantage' ) => 'auto',
						esc_html__('Keep height', 'avantage' ) => 'keep-height',
						esc_html__('Half screen', 'avantage' ) => 'half_screen',
						esc_html__('Full screen', 'avantage' ) => 'full_screen'
					)
				),
				array( 'param_name' => 'animation', 'type' => 'dropdown', 'heading' => esc_html__( 'Animation', 'avantage' ), 'description' => esc_html__( 'If fade is selected, number of slides to show will be 1', 'avantage' ),
					'value' => array(
						esc_html__('Default', 'avantage' ) => 'slide',
						esc_html__('Fade', 'avantage' ) => 'fade'
					)
				),
				array( 'param_name' => 'show_arrows', 'default' => 'show', 'type' => 'dropdown', 'heading' => esc_html__( 'Navigation arrows', 'avantage' ), 'group' => esc_html__( 'Design', 'avantage' ),
					'value' => array(
						esc_html__('Show', 'avantage' ) => 'show',
						esc_html__('Hide', 'avantage' ) => 'hide'
					)
				),
				array( 'param_name' => 'arrows_size', 'type' => 'dropdown', 'preview' => true, 'default' => 'normal', 'heading' => esc_html__( 'Navigation arrows size', 'avantage' ), 'group' => esc_html__( 'Design', 'avantage' ),
					'value' => array(
						esc_html__('Small', 'avantage' ) => 'small',
						esc_html__('Normal', 'avantage' ) => 'normal',
						esc_html__('Large', 'avantage' ) => 'large'
					)
				),
				array( 'param_name' => 'arrows_style', 'type' => 'dropdown', 'preview' => true, 'default' => 'normal', 'heading' => esc_html__( 'Navigation arrows style', 'avantage' ), 'group' => esc_html__( 'Design', 'avantage' ),
					'value' => array(
						esc_html__('Default', 'avantage' ) => '',
						esc_html__('Transparent background, light arrow', 'avantage' ) => 'transparent_light',
						esc_html__('Transparent background, dark arrow', 'avantage' ) => 'transparent_dark',
						esc_html__('Accent background, light arrow', 'avantage' ) => 'accent_light',
						esc_html__('Accent background, dark arrow', 'avantage' ) => 'accent_dark',
						esc_html__('Alternate background, light arrow', 'avantage' ) => 'alternate_light',
						esc_html__('Alternate background, dark arrow', 'avantage' ) => 'alternate_dark'
					)
				),
				array( 'param_name' => 'arrows_position', 'type' => 'dropdown', 'preview' => true, 'default' => 'normal', 'heading' => esc_html__( 'Navigation arrows position', 'avantage' ), 'group' => esc_html__( 'Design', 'avantage' ),
					'value' => array(
						esc_html__('At sides', 'avantage' ) => '',
						esc_html__('From outside, at sides', 'avantage' ) => 'outside',
						esc_html__('Bottom left', 'avantage' ) => 'bottom_left',
						esc_html__('Bottom right', 'avantage' ) => 'bottom_right',
						esc_html__('Below', 'avantage' ) => 'below',
						esc_html__('Below left', 'avantage' ) => 'below_left',
						esc_html__('Below right', 'avantage' ) => 'below_right'
					)
				),
				array( 'param_name' => 'show_dots', 'type' => 'dropdown', 'heading' => esc_html__( 'Dots navigation', 'avantage' ), 'group' => esc_html__( 'Design', 'avantage' ),
					'value' => array(
						esc_html__('Bottom', 'avantage' ) => 'bottom',
						esc_html__('Below', 'avantage' ) => 'below',
						esc_html__('Left', 'avantage' ) => 'left',
						esc_html__('Right', 'avantage' ) => 'right',
						esc_html__('Hide', 'avantage' ) => 'hide'
					)
				),
				array( 'param_name' => 'dots_style', 'type' => 'dropdown', 'heading' => esc_html__( 'Dots style', 'avantage' ), 'group' => esc_html__( 'Design', 'avantage' ),
					'value' => array(
						esc_html__('Inherit', 'avantage' ) => '',
						esc_html__('Accent active dot', 'avantage' ) => 'accent_dot',
						esc_html__('Alternate active dot', 'avantage' ) => 'alternate_dot',
						esc_html__('Light active dot', 'avantage' ) => 'light_dot',
						esc_html__('Dark active dot', 'avantage' ) => 'dark_dot'
					)
				),
				array( 'param_name' => 'slides_to_show', 'type' => 'textfield', 'preview' => true, 'default' => 1, 'heading' => esc_html__( 'Number of slides to show', 'avantage' ), 'description' => esc_html__( 'e.g. 3', 'avantage' ) ),
				array( 'param_name' => 'auto_play', 'type' => 'textfield', 'heading' => esc_html__( 'Autoplay interval (ms)', 'avantage' ), 'description' => esc_html__( 'e.g. 2000', 'avantage' ) )
			)
		) );
	}
}