<?php

class bt_bb_service extends BT_BB_Element {

	function handle_shortcode( $atts, $content ) {
		extract( shortcode_atts( apply_filters( 'bt_bb_extract_atts', array(
			'icon'         => '',
			'title'        => '',
			'text'         => '',
			'url'          => '',
			'target'       => '',
			'color_scheme' => '',
			'style'        => '',
			'size'         => '',
			'shape'        => '',
			'align'        => '',
			'highlight'        => ''
		) ), $atts, $this->shortcode ) );

		$class = array( $this->shortcode );

		if ( $el_class != '' ) {
			$class[] = $el_class;
		}

		$id_attr = '';
		if ( $el_id != '' ) {
			$id_attr = ' ' . 'id="' . esc_attr( $el_id ) . '"';
		}

		$style_attr = '';
		if ( $el_style != '' ) {
			$style_attr = ' ' . 'style="' . esc_attr( $el_style ). '"';
		}
		
		if ( $color_scheme != '' ) {
			$class[] = $this->prefix . 'color_scheme_' . bt_bb_get_color_scheme_id( $color_scheme );
		}		

		if ( $style != '' ) {
			$class[] = $this->prefix . 'style' . '_' . $style;
		}

		if ( $size != '' ) {
			$class[] = $this->prefix . 'size' . '_' . $size;
		}

		if ( $shape != '' ) {
			$class[] = $this->prefix . 'shape' . '_' . $shape;
		}
		
		if ( $align != '' ) {
			$class[] = $this->prefix . 'align' . '_' . $align;
		}

		if ( $highlight != '' ) {
			$class[] = $this->prefix . 'highlight_service' . '_' . $highlight;
		}
		
		if ( $url != '' ) {
			$title = '<a href="' . esc_attr( $url ) . '" target="' . esc_attr( $target ) . '">' . $title . '</a>';
		} 

		$icon = bt_bb_icon::get_html( $icon, '', $url, $target );
		
		$class = apply_filters( $this->shortcode . '_class', $class, $atts );

		$output = $icon;

		$output .= '<div class="' . esc_attr( $this->shortcode . '_content' ) . '">';
			$output .= '<div class="' . esc_attr( $this->shortcode . '_content_title' ) . '">' . $title . '</div>';
			$output .= '<div class="' . esc_attr( $this->shortcode . '_content_text' ) . '">' . nl2br( $text ) . '</div>';
		$output .= '</div>';

		$output = '<div' . $id_attr . ' class="' . implode( ' ', $class ) . '"' . $style_attr . '>' . $output . '</div>';

		
		$output = apply_filters( 'bt_bb_general_output', $output, $atts );
		$output = apply_filters( $this->shortcode . '_output', $output, $atts );
		
		return $output;
	}

	function map_shortcode() {

		if ( function_exists('boldthemes_get_icon_fonts_bb_array') ) {
			$icon_arr = boldthemes_get_icon_fonts_bb_array();
		} else {
			require_once( WP_PLUGIN_DIR . '/bold-page-builder/content_elements_misc/s7_icons.php' );
			require_once( WP_PLUGIN_DIR . '/bold-page-builder/content_elements_misc/fa_icons.php' );
			$icon_arr = array( 'Font Awesome' => bt_bb_fa_icons(), 'S7' => bt_bb_s7_icons() );
		}

		$color_scheme_arr = bt_bb_get_color_scheme_param_array();

		bt_bb_map( $this->shortcode, array( 'name' => esc_html__( 'Service', 'avantage' ), 'description' => esc_html__( 'Icon with text', 'avantage' ), 'icon' => $this->prefix_backend . 'icon' . '_' . $this->shortcode,
			'params' => array(
				array( 'param_name' => 'icon', 'type' => 'iconpicker', 'heading' => esc_html__( 'Icon', 'avantage' ), 'value' => $icon_arr, 'preview' => true ),
				array( 'param_name' => 'title', 'type' => 'textfield', 'heading' => esc_html__( 'Title', 'avantage' ), 'preview' => true ),
				array( 'param_name' => 'text', 'type' => 'textarea', 'heading' => esc_html__( 'Text', 'avantage' ) ),
				array( 'param_name' => 'url', 'type' => 'textfield', 'heading' => esc_html__( 'URL', 'avantage' ) ),
				array( 'param_name' => 'target', 'type' => 'dropdown', 'heading' => esc_html__( 'Target', 'avantage' ),
					'value' => array(
						esc_html__('Self (open in same tab)', 'avantage' ) => '_self',
						esc_html__('Blank (open in new tab)', 'avantage' ) => '_blank',
					)
				),
				array( 'param_name' => 'align', 'type' => 'dropdown', 'heading' => esc_html__( 'Icon alignment', 'avantage' ),
					'value' => array(
						esc_html__('Inherit', 'avantage' ) => 'inherit',
						esc_html__('Left', 'avantage' ) => 'left',
						esc_html__('Right', 'avantage' ) => 'right'
					)
				),
				array( 'param_name' => 'size', 'type' => 'dropdown', 'heading' => esc_html__( 'Icon size', 'avantage' ), 'preview' => true, 'group' => esc_html__( 'Design', 'avantage' ),
					'value' => array(
						esc_html__('Small', 'avantage' ) => 'small',
						esc_html__('Extra small', 'avantage' ) => 'xsmall',
						esc_html__('Normal', 'avantage' ) => 'normal',
						esc_html__('Large', 'avantage' ) => 'large',
						esc_html__('Extra large', 'avantage' ) => 'xlarge'
					)
				),
				array( 'param_name' => 'color_scheme', 'type' => 'dropdown', 'heading' => esc_html__( 'Color scheme', 'avantage' ), 'value' => $color_scheme_arr, 'preview' => true, 'group' => esc_html__( 'Design', 'avantage' ) ),
				array( 'param_name' => 'style', 'type' => 'dropdown', 'heading' => esc_html__( 'Icon style', 'avantage' ), 'preview' => true, 'group' => esc_html__( 'Design', 'avantage' ),
					'value' => array(
						esc_html__('Outline', 'avantage' ) => 'outline',
						esc_html__('Filled', 'avantage' ) => 'filled',
						esc_html__('Borderless', 'avantage' ) => 'borderless'
					)
				),
				array( 'param_name' => 'shape', 'type' => 'dropdown', 'heading' => esc_html__( 'Icon shape', 'avantage' ), 'group' => esc_html__( 'Design', 'avantage' ),
					'value' => array(
						esc_html__('Circle', 'avantage' ) => 'circle',
						esc_html__('Square', 'avantage' ) => 'square',
						esc_html__('Rounded Square', 'avantage' ) => 'round',
						esc_html__('Slanted Right', 'avantage' ) => 'slanted_right',
						esc_html__('Slanted Left', 'avantage' ) => 'slanted_left'
					)
				),
				array( 'param_name' => 'highlight', 'type' => 'checkbox', 'value' => array( esc_html__( 'Yes', 'avantage' ) => 'true' ), 'heading' => esc_html__( 'Highlight service', 'avantage' ), 'preview' => true, 'group' => esc_html__( 'Design', 'avantage' ),
				),			)
		) );
	}
}