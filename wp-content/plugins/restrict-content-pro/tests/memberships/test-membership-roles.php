<?php
/**
 * Membership Roles Tests
 *
 * Performs tests on how user roles work with membership records.
 *
 * @package   restrict-content-pro
 * @copyright Copyright (c) 2020, Sandhills Development, LLC
 * @license   GPL2+
 */

namespace RCP\Tests;


/**
 * Class Membership_Role_Tests
 *
 * @package RCP\Tests
 */
class Membership_Role_Tests extends UnitTestCase {

	/**
	 * Membership level that grants "Editor" role
	 *
	 * @var object
	 */
	protected static $level_editor;

	/**
	 * Membership level that grants "Administrator" role
	 *
	 * @var object
	 */
	protected static $level_administrator;

	/**
	 * Set up once before tests are run
	 */
	public static function setUpBeforeClass() {
		parent::setUpBeforeClass();

		// Set up a membership level that grants the "Editor" role.
		self::$level_editor = self::rcp()->level->create_and_get( array(
			'role' => 'editor'
		) );

		// Set up a membership level that grants the "Administrator" role.
		self::$level_administrator = self::rcp()->level->create_and_get( array(
			'role' => 'administrator'
		) );
	}

	/**
	 * Adding a pending membership should not grant the associated role.
	 */
	public function test_pending_membership_doesnt_grant_role() {

		$customer = self::rcp()->customer->create_and_get();

		$customer->add_membership( array(
			'object_id' => self::$level_editor->id,
			'status'    => 'pending'
		) );

		$user = get_userdata( $customer->get_user_id() );

		$this->assertFalse( in_array( 'editor', $user->roles ) );

	}

	/**
	 * Adding an active membership should grant the associated role.
	 *
	 * @covers \RCP_Membership::add_user_role
	 */
	public function test_activating_membership_grants_role() {

		$customer = self::rcp()->customer->create_and_get();

		$customer->add_membership( array(
			'object_id' => self::$level_editor->id,
			'status'    => 'active'
		) );

		$user = get_userdata( $customer->get_user_id() );

		$this->assertTrue( in_array( 'editor', $user->roles ) );

	}

	/**
	 * Disabling a membership should remove the role that was granted.
	 *
	 * @covers \RCP_Membership::disable
	 */
	public function test_disabling_membership_removes_role() {

		$customer = self::rcp()->customer->create_and_get();

		$membership_id = $customer->add_membership( array(
			'object_id' => self::$level_editor->id,
			'status'    => 'active'
		) );

		$user = get_userdata( $customer->get_user_id() );

		$this->assertTrue( in_array( 'editor', $user->roles ) );

		$membership = rcp_get_membership( $membership_id );
		$membership->disable();

		$user = get_userdata( $customer->get_user_id() );

		$this->assertFalse( in_array( 'editor', $user->roles ) );

	}

	/**
	 * Expiring a membership should remove the role that was granted.
	 *
	 * @covers \RCP_Membership::expire
	 */
	public function test_expiring_membership_removes_role() {

		$customer = self::rcp()->customer->create_and_get();

		$membership_id = $customer->add_membership( array(
			'object_id' => self::$level_editor->id,
			'status'    => 'active'
		) );

		$user = get_userdata( $customer->get_user_id() );

		$this->assertTrue( in_array( 'editor', $user->roles ) );

		$membership = rcp_get_membership( $membership_id );
		$membership->expire();

		$user = get_userdata( $customer->get_user_id() );

		$this->assertFalse( in_array( 'editor', $user->roles ) );

	}

	/**
	 * Disabling a membership should never remove the administrator role.
	 *
	 * @covers \RCP_Membership::disable
	 */
	public function test_disabling_membership_doesnt_remove_administrator_role() {

		$customer = self::rcp()->customer->create_and_get();

		$membership_id = $customer->add_membership( array(
			'object_id' => self::$level_administrator->id,
			'status'    => 'active'
		) );

		$user = get_userdata( $customer->get_user_id() );

		$this->assertTrue( in_array( 'administrator', $user->roles ) );

		$membership = rcp_get_membership( $membership_id );
		$membership->disable();

		$user = get_userdata( $customer->get_user_id() );

		$this->assertTrue( in_array( 'administrator', $user->roles ) );

	}

	/**
	 * Expiring a membership should never remove the administrator role.
	 *
	 * @covers \RCP_Membership::expire
	 */
	public function test_expiring_membership_doesnt_remove_administrator_role() {

		$customer = self::rcp()->customer->create_and_get();

		$membership_id = $customer->add_membership( array(
			'object_id' => self::$level_administrator->id,
			'status'    => 'active'
		) );

		$user = get_userdata( $customer->get_user_id() );

		$this->assertTrue( in_array( 'administrator', $user->roles ) );

		$membership = rcp_get_membership( $membership_id );
		$membership->expire();

		$user = get_userdata( $customer->get_user_id() );

		$this->assertTrue( in_array( 'administrator', $user->roles ) );

	}

}
