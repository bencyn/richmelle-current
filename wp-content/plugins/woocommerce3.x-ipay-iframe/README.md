# Wordpress plugin for ipay integration
------------------------------------------
This plugin helps in the quick integration with woocommerce 2.5 and above.
@author kiarie: technical@ipayafrica.com

Copy the zip file to wp_contents/plugins and unzip
copy the wp_ipay.php file in that folder
i.e. wp_contents/plugins
-----------------------------------------------
Go to the plugins administration enable it and set the desired parameters and your good to go!
-------------------------------------------------------

